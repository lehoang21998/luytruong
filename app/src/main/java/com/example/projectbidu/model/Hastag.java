package com.example.projectbidu.model;

public class Hastag {
    private String nameHastag;

    public Hastag(String nameHastag) {
        this.nameHastag = nameHastag;
    }

    public String getNameHastag() {
        return nameHastag;
    }

    public void setNameHastag(String nameHastag) {
        this.nameHastag = nameHastag;
    }
}
