package com.example.projectbidu.model;

public class Seller {
    private int idSeller;
    private String userName;
    private int imageSeller;
    private int rankSeller;
    private String category;
    private String hasTag;

    public Seller(int idSeller, String userName, int imageSeller, int rankSeller, String category, String hasTag) {
        this.idSeller = idSeller;
        this.userName = userName;
        this.imageSeller = imageSeller;
        this.rankSeller = rankSeller;
        this.category = category;
        this.hasTag = hasTag;
    }

    public Seller(String userName, int imageSeller, int rankSeller, String category, String hasTag) {
        this.userName = userName;
        this.imageSeller = imageSeller;
        this.rankSeller = rankSeller;
        this.category = category;
        this.hasTag = hasTag;
    }

    public int getIdSeller() {
        return idSeller;
    }

    public void setIdSeller(int idSeller) {
        this.idSeller = idSeller;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getImageSeller() {
        return imageSeller;
    }

    public void setImageSeller(int imageSeller) {
        this.imageSeller = imageSeller;
    }

    public int getRankSeller() {
        return rankSeller;
    }

    public void setRankSeller(int rankSeller) {
        this.rankSeller = rankSeller;
    }

    public String getHasTag() {
        return hasTag;
    }

    public void setHasTag(String hasTag) {
        this.hasTag = hasTag;
    }
}
