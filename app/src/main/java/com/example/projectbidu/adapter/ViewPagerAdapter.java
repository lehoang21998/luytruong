package com.example.projectbidu.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.projectbidu.view.Fragment_All;
import com.example.projectbidu.view.Fragment_TopSellers;

public class ViewPagerAdapter extends FragmentStateAdapter {

    public ViewPagerAdapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new Fragment_All();
            case 1:
                return new Fragment_TopSellers();
            default:
                return new Fragment_All();
        }
    }

    @Override
    public int getItemCount() {
        return 2;
    }
}