package com.example.projectbidu.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.projectbidu.R;
import com.example.projectbidu.model.Hastag;

import java.util.List;

public class AdapterHastag extends RecyclerView.Adapter<AdapterHastag.HastagViewHodel> {
    private List<Hastag> mListHastag;

    public AdapterHastag(List<Hastag> mListHastag) {
        this.mListHastag = mListHastag;
    }

    @NonNull
    @Override
    public HastagViewHodel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_hastag, parent,false);
        return new HastagViewHodel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HastagViewHodel holder, int position) {
        Hastag hastag = mListHastag.get(position);
        holder.tvHastag.setText(hastag.getNameHastag());
    }

    @Override
    public int getItemCount() {
        return mListHastag.size();
    }

    public class HastagViewHodel extends RecyclerView.ViewHolder {
        private TextView tvHastag;

        public HastagViewHodel(@NonNull View itemView) {
            super(itemView);
            tvHastag = itemView.findViewById(R.id.tvHastag);

        }

    }
}
