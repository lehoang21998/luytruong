package com.example.projectbidu.viewmodel;

import android.util.Log;

import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.projectbidu.R;
import com.example.projectbidu.model.Category;
import com.example.projectbidu.model.Product;
import com.example.projectbidu.view.Fragment_All;

import java.util.ArrayList;
import java.util.List;

public class ProductTopViewModel extends ViewModel {
    private MutableLiveData<List<Product>> mListProductLiveData;
    private MutableLiveData<Product> productLivedata;
    private MutableLiveData<List<Product>> mListDataFitter;
    private List<Product> mListProduct;


    public void setListProduct(List<Product> mListProduct) {
        this.mListProduct = mListProduct;
    }

    public ProductTopViewModel() {
        mListProductLiveData = new MutableLiveData<>();
        productLivedata = new MutableLiveData<>();
        mListDataFitter =new MutableLiveData<>();
        initData();
    }

    private void initData() {
        mListProduct = new ArrayList<>();
        mListProduct.add(new Product(1,R.drawable.productdemo,false,"[Woman] Trousers short","Jacket",450000L,600000L,true,15L));
        mListProduct.add(new Product(2,R.drawable.anh2,false,"[Code FASHIONHOT27 reduc 10K] Freeship 50K- Trousers","Sweater",450000L,null,false,15L));
        mListProduct.add(new Product(3,R.drawable.anh1,true,"[Woman] Trousers short","Jacket",450000L,null,true,15L));
        mListProduct.add(new Product(4,R.drawable.anh3,false,"[Code FASHIONHOT27 reduc 10K] Freeship 50K- Trousers","Blouse",450000L,null,true,15L));
        mListProduct.add(new Product(5,R.drawable.productdemo,true,"[Woman] Trousers short","Skinny pants",450000L,600000L,true,null));
        mListProduct.add(new Product(6,R.drawable.anh2,false,"[Code FASHIONHOT27 reduc 10K] Freeship 50K- Trousers","Blouse",450000L,null,false,15L));
        mListProduct.add(new Product(7,R.drawable.anh1,true,"[Woman] Trousers short","Sweater",450000L,null,true,15L));
        mListProduct.add(new Product(8,R.drawable.anh3,false,"[Code FASHIONHOT27 reduc 10K] Freeship 50K- Trousers","Skinny pants",450000L,null,true,null));
        mListProduct.add(new Product(9,R.drawable.productdemo,false,"[Woman] Trousers short","Sweater",450000L,600000L,true,15L));
        mListProductLiveData.setValue(mListProduct);
    }



    public MutableLiveData<List<Product>> getListProductLiveData() {
        return mListProductLiveData;
    }

    public MutableLiveData<Product> getProduct() {
        return productLivedata;
    }

    public MutableLiveData<List<Product>> getListDataFitter() {
        return mListDataFitter;
    }

    public void updateProduct(Product product) {
        for (Product item :mListProduct) {
            if(item.getIdProduct() == product.getIdProduct()){
                if(item.isLike()) item.setLike(false);
                else item.setLike(true);
                productLivedata.setValue(item);
            }
        }
    }

    public void resetListProduct(Category category) {
        if(category.getTitle().equals("All")){
            mListProductLiveData.setValue(mListProduct);
        }else{
            List<Product> temp = new ArrayList<>();
            for (int i=0;i<mListProduct.size();i++){
                if(mListProduct.get(i).getCategory().equals(category.getTitle())){
                    temp.add(mListProduct.get(i));
                }
            }
            mListProductLiveData.setValue(temp);
        }
    }
}
