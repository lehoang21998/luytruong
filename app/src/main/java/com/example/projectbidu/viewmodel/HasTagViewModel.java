package com.example.projectbidu.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.projectbidu.model.Category;
import com.example.projectbidu.model.Hastag;

import java.util.ArrayList;
import java.util.List;

public class HasTagViewModel extends ViewModel {
    private MutableLiveData<List<Hastag>> mListHastagLiveData;
    private List<Hastag> mListHastag;

    public HasTagViewModel() {
        mListHastagLiveData = new MutableLiveData<>();
        initData();
    }

    private void initData() {
        mListHastag = new ArrayList<>();
        mListHastag.add(new Hastag("#happy"));
        mListHastag.add(new Hastag("#clothes"));

        mListHastagLiveData.setValue(mListHastag);
    }

    public LiveData<List<Hastag>> getListHastagLiveData() {
        return mListHastagLiveData;
    }
}
