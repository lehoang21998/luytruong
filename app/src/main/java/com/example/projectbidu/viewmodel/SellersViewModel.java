package com.example.projectbidu.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.projectbidu.R;
import com.example.projectbidu.model.Category;
import com.example.projectbidu.model.Seller;

import java.util.ArrayList;
import java.util.List;

public class SellersViewModel extends ViewModel {
    private MutableLiveData<List<Seller>> mListSellersLiveData;
    private List<Seller> mListSellers;

    public SellersViewModel() {
         mListSellersLiveData = new MutableLiveData<>();
         initData();
    }

    private void initData() {
        mListSellers = new ArrayList<>();
        mListSellers.add(new Seller(1,"Person Mandaley", R.drawable.sellerdemo,1,"Sweater","#Jacket #Sweater #Skinny pants #Blouse #Jeans #<16"));
        mListSellers.add(new Seller(2,"Tuyen Nguyen Kim",R.drawable.anh22,2,"Jacket","#Jacket #Sweater #Jeans #<16"));
        mListSellers.add(new Seller(3,"Veray Rose",R.drawable.anh11,3,"Skinny pants","#Jacket #Sweater #Skinny pants"));
        mListSellers.add(new Seller(4,"Angel Angel",R.drawable.anh33,4,"Jacket","#Jacket #Sweater #Skinny pants #Blouse #Jeans #<16"));
        mListSellers.add(new Seller(5,"Person Mandaley",R.drawable.sellerdemo,5,"Blouse","#Jacket #Sweater  #Blouse"));
        mListSellers.add(new Seller(6,"Tuyen Nguyen Kim",R.drawable.anh22,6,"Blouse","#Jacket #Sweater #Skinny pants #Blouse #Jeans #<16"));
        mListSellers.add(new Seller(7,"Veray Rose",R.drawable.anh11,7,"Jacket","#Jacket #Sweater #Skinny pants  #Jeans #<16"));
        mListSellers.add(new Seller(8,"Angel Angel",R.drawable.anh33,8,"Skinny pants","#Jacket #Sweater #<16"));
        mListSellersLiveData.setValue(mListSellers);
    }

    public LiveData<List<Seller>> getmListSellersLiveData() {
        return mListSellersLiveData;
    }

    public void resetListSellers(Category category) {
        if(category.getTitle().equals("All")){
            mListSellersLiveData.setValue(mListSellers);
        }else{
            List<Seller> temp = new ArrayList<>();
            for (int i=0;i<mListSellers.size();i++){
                if(mListSellers.get(i).getCategory().equals(category.getTitle())){
                    temp.add(mListSellers.get(i));
                }
            }
            mListSellersLiveData.setValue(temp);
        }
    }

}
